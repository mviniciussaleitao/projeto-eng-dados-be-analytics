"""BEM VINDO A VERSÃO ALTERNATIVA DA FUNÇÃO DE INSEERT.
ESSA VERSÃO, TEM COMO VANTAGENS VERFICAR A CADA INSERT SE O DADO JÁ EXISTE E EM SEGUIDA CASO POSITIVO APENAS UM UPDATE É GERADO.
COMO CONSEQUÊNCIA , TEMOS UM PROCESSO MAIS SEGURO, PORÉM CONSIDERAVELMENTE MAIS LENTO"""
from google.cloud import bigquery



def inserir(Nome, Percentual_desconto, Preço, Avaliação, Data_lançamento, Fim_promo, Inicio_promo, Data_inicio_promo,
            Data_fim_promo):
    try:
        # Construa um objeto cliente BigQuery.
        client = bigquery.Client(project="pacific-decoder-423701-q8")

        query = """
           MERGE `pacific-decoder-423701-q8.BE_ANALYTCS.Dados_SteamDB` AS tabela_steamdb
           USING (
               SELECT 
               @Nome AS `Nome`, 
               @Percentual_desconto AS `Percentual_desconto`,
               @Preco AS `Preço`,
               @Avaliacao AS `Avaliação`,
               @Data_lancamento AS `Data_lançamento`,
               @Fim_promo AS `Fim_promo`,
               @Inicio_promo AS `Inicio_promo`,
               @Data_inicio_promo AS `Data_inicio_promo`,
               @Data_fim_promo AS `Data_fim_promo`
           ) AS tab_temporaria
           ON tabela_steamdb.`Nome` = tab_temporaria.`Nome` and tabela_steamdb.`Data_inicio_promo` = tab_temporaria.`Data_inicio_promo`
           WHEN MATCHED THEN
               UPDATE SET 
                   tabela_steamdb.`Fim_promo` = tab_temporaria.`Fim_promo`,
                   tabela_steamdb.`Inicio_promo` = tab_temporaria.`Inicio_promo`
           WHEN NOT MATCHED THEN
                INSERT (`Nome`, `Percentual_desconto`, `Preço`, `Avaliação`, `Data_lançamento`, `Fim_promo`, `Inicio_promo`, `Data_inicio_promo`, `Data_fim_promo`) 
               VALUES (tab_temporaria.`Nome`, tab_temporaria.`Percentual_desconto`, tab_temporaria.`Preço`, tab_temporaria.`Avaliação`, tab_temporaria.`Data_lançamento`, tab_temporaria.`Fim_promo`, tab_temporaria.`Inicio_promo`, tab_temporaria.`Data_inicio_promo`, tab_temporaria.`Data_fim_promo`)
           """

        # Defina os parâmetros da consulta
        job_config = bigquery.QueryJobConfig(
            query_parameters=[
                bigquery.ScalarQueryParameter("Nome", "STRING", Nome),
                bigquery.ScalarQueryParameter("Percentual_desconto", "STRING", Percentual_desconto),
                bigquery.ScalarQueryParameter("Preco", "STRING", Preço),
                bigquery.ScalarQueryParameter("Avaliacao", "STRING", Avaliação),
                bigquery.ScalarQueryParameter("Data_lancamento", "STRING", Data_lançamento),
                bigquery.ScalarQueryParameter("Fim_promo", "STRING", Fim_promo),
                bigquery.ScalarQueryParameter("Inicio_promo", "STRING", Inicio_promo),
                bigquery.ScalarQueryParameter("Data_inicio_promo", "STRING", Data_inicio_promo),
                bigquery.ScalarQueryParameter("Data_fim_promo", "STRING", Data_fim_promo)
            ]
        )

        # Executa a consulta.
        query_job = client.query(query, job_config=job_config)
        query_job.result()  # Aguarda a conclusão da declaração

        print("Operação MERGE concluída com sucesso!")

    except Exception as e:
        print(f"Erro ao executar a operação MERGE: {e}")