#import versao_alternativa_insert
from bigqueryfunc import inserir
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import Select
import time

def scroll_gradual(navegador, quantidade, intervalo):
    # Obtém a altura da janela do navegador
    altura_janela = navegador.get_window_size()['height']

    # Roda a página em incrementos
    for i in range(0, quantidade, altura_janela):
        navegador.execute_script(f"window.scrollTo(0, {i});")
        time.sleep(intervalo)

def extracao():
    opcoes_selenium_firefox = Options()
    opcoes_selenium_firefox.add_argument('--headless')  # Essa parte faz com que o navegador não precisa da interface gráfica pra funcionar

    try:
        navegador = webdriver.Firefox(options=opcoes_selenium_firefox)
        navegador.get("https://steamdb.info/sales/")
        navegador.maximize_window()
        navegador.implicitly_wait(1)  # Adiciona uma espera implícita de até 1 s para os elementos aparecerem

        selecionar_elemento_paginacao = navegador.find_element(By.XPATH, """//*[@id="dt-length-0"]""")
        selecionar_da_lista_do_elemento_pag = Select(selecionar_elemento_paginacao)
        selecionar_da_lista_do_elemento_pag.select_by_value("-1")

        altura_pagina = navegador.find_element(By.TAG_NAME, "body").get_property("scrollHeight")
        scroll_gradual(navegador, altura_pagina, 0.5)

        tabela = navegador.find_element(By.XPATH, '//*[@id="DataTables_Table_0"]')
        linhas = tabela.find_elements(By.XPATH, './tbody/tr')
        dados_tabela = []

        for linha in linhas:
            try:
                celulas = linha.find_elements(By.TAG_NAME, 'td')
                nome_e_sub_info = celulas[2]
                name = nome_e_sub_info.find_element(By.CLASS_NAME, 'b').text.strip()
                discount_percentage = celulas[3].text.strip()
                price = celulas[4].text.strip()
                rating = celulas[5].text.strip()
                release = celulas[6].text.strip()
                ends = celulas[7].text.strip()
                dt_fim = celulas[7].get_attribute("title").strip()
                started = celulas[8].text.strip()
                dt_inicio = celulas[8].get_attribute("title").strip()
                dados_tabela.append({
                    'Nome': name,
                    'Percentual_desconto': discount_percentage,
                    'Preço': price,
                    'Avaliação': rating,
                    'Data_lançamento': release,
                    'Fim_promo': ends,
                    'Inicio_promo': started,
                    'Data_inicio_promo': dt_inicio,
                    'Data_fim_promo': dt_fim
                })
            except Exception as e:
                print(f"Erro ao processar linha: {e}")
        try:
            inserir(dados_tabela) # nessa parte chamamos a funcao para inserir todos os dados coletados
        except Exception as e:
            print(f"Erro ao inserir dados no BigQuery: {e}")

    except Exception as e:
        print(f"Erro ao inicializar o navegador: {e}")
    finally:
        navegador.quit()
extracao()



"""PARA O CASO DE QUERER USAR A VERSÂO ALTERNATIVA DE INSERÇÂO ESSE TRECHO DEVE SUBSTITUIR O TRECHO DE INSERIR OS DADOS COLETADOS
ALÈM DISSO DEVE_SE DESCOMENTAR O IMPORT DA VERSÂO ALTERNATIVA"""

# for dado in dados_tabela:
#     try:
#         versao_alternativa_insert.inserir(
#             Nome=dado["Nome"],
#             Percentual_desconto=dado["Percentual_desconto"],
#             Preço=dado["Preço"],
#             Avaliação=dado["Avaliação"],
#             Data_lançamento=dado["Data_lançamento"],
#             Fim_promo=dado["Fim_promo"],
#             Inicio_promo=dado["Inicio_promo"],
#             Data_inicio_promo=dado["Data_inicio_promo"],
#             Data_fim_promo=dado["Data_fim_promo"]
#         )
#     except Exception as e:
#         print(f"Erro ao inserir dados no BigQuery: {e}")