from google.cloud import bigquery

def inserir(dados):
    try:
        # Construa um objeto cliente BigQuery.
        client = bigquery.Client(project="pacific-decoder-423701-q8")

        # Referência para a tabela no BigQuery
        table_id = "pacific-decoder-423701-q8.BE_ANALYTCS.Dados_SteamDB"

        # Inserir os dados
        errors = client.insert_rows_json(table_id, dados)

        if errors == []:
            print("Todos os dados foram inseridos com sucesso.")
        else:
            print("Erros ao inserir dados:", errors)

    except Exception as e:
        print(f"Ocorreu um erro inesperado: {e}")
